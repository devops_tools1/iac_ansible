# Iac Ansible

# Repositorio de ejemplos de Ansible

>  <p style='text-align: justify;'> En el siguiente repositorio se tiene ejemplos sencillos de como utilizar Ansible como una herramienta para el manejo de configuracion, esta herramienta ha sido utilizada en diferentes contexto con el fin de alcanzar la automatizacion en la configuracion de la infraestructura </p>


## Antes de empezar

<p style='text-align: justify;'> Para los siguientes ejemplos estan pensados para ser probados utilizando un contenedor de Docker, por lo que se provee el dockerfile necesario para poder instalar dependencias y todo lo que se necesita para poder correr y probar cada uno de los ejemplo. Si quieres mayor informacion de como instalar docker en tu computadora local puedes dirigirte al siguiente enlace: </p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

### Como correr los ejemplos

<p style='text-align: justify;'> 
Para poder correr los ejemplos, este repositorio cuenta con un Dockerfile que permite crear el contenedor con el ambiente ya preparado para poder probar los ejemplos. Para ejecutarlos puedes seguir los siguientes pasos
</p>

1. Construye el contenedor de Docker

```bash
$ docker build -t <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR> .
```

2. Corre el contenedor y monta el directorio de **playbooks** en el contenedor

```bash
$ docker run -it --rm -v $PWD/playbooks:/var/ansible/playbooks <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR> bash
```

3. Ahora te encontraras en el contenedor y ya puedes probar los ejemplos:

```bash
root@34add936c278:/var/ansible/playbooks$ ansible-playbook -c local -i localhost ejemplo1.yml
```

## Ejemplos

<p style='text-align: justify;'> Cada uno de los ejemplos busca dar una idea general de la capacida de Ansible, es importante aclarar que no se cubre en la totalidad la funcionalidad de la herramienta, es mas para dar un introduccion de lo que se puede crear utilizando este tipo de herramientas. </p>

### Ejemplo 1

<p style='text-align: justify;'> En este primer ejemplo buscamos presentar la funcionalidad de la herramienta, para ello vamos a presentar el siguiente ejercicio: </p>

> <p style='text-align: justify;'>  Para el primer ejemplo se necesita automatizar la tarea de utilizar Ansible para poder crear las siguientes configuraciones en un servidor con el sistema operativo Ubuntu 18.04:
</p>

* Crear una carpeta en el directorio **home** con el nombre **ejemplo1**

* En la carpeta creada se debde de existir un archivo de configuracion con extension **.txt**.

* El servidor debe de tener el paquete de **git** instalado.

**Solucion:** [Role de Ansible: ejemplo1](playbooks/ejemplo1/tasks/main.yml)

### Ejemplo 2

<p style='text-align: justify;'> En este segundo ejemplo buscamos presentar la funcionalidad de la herramienta, para ello vamos a presentar el siguiente ejercicio: </p>

> <p style='text-align: justify;'>  Se necesita automatizar la tarea de utilizar Ansible para poder crear las siguientes configuraciones en un servidor con el sistema operativo Ubuntu 18.04:
</p>

1. Crear una carpeta en el directorio **home** utilizando atributos pasados al servidor, es decir que se deben parametrizar: nombre y permisos de la carpeta

2. Se debe de crear un archivo en **home** con configuraciones, utilizando variables, se deben parametrizar: el nombre, los permisos y el contenido del archivo

3. Se debe de crear un archivo estatico hacia la carpeta **home** del servidor, esto utilizando el directorio **files** que ofrece Ansible


4. Instalar todos los paquetes que se especifiquen por medio de un arreglo en al archivo de variables de Ansible, estos pueden ser uno o mas paquetes que se necesiten instalar en el servidor.

**Solucion:** [Role de Ansible: ejemplo2](playbooks/ejemplo2/tasks/main.yml)

### Youtube Playlist

* [Playlist de video ejemplos](https://youtube.com/playlist?list=PL8Xnw9lMxPCGblGw2b3W9EZUC45ARNJLR)
