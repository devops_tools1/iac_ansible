FROM ubuntu:18.04

RUN apt update

RUN apt install -y ansible

RUN mkdir /var/ansible

RUN mkdir /var/ansible/playbooks

WORKDIR /var/ansible/playbooks