#!/bin/bash

docker build -t ansible:v1 .

docker run -it --rm -v $PWD/playbooks:/var/ansible/playbooks ansible:v1 bash